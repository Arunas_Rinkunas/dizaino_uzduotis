
<?php
session_start();


if (empty($_POST["full_name"])) { //checking name if it's there or not
    $full_nameErr = "Name is required.";
  } else {
    $full_name = test_input($_POST["full_name"]);    //when we have a name send to function for removing spaces, slashes, etc.
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$full_name)) {
      $full_nameErr = "Only letters and white space allowed."; 
    }
  }


  if (empty($_POST["email"])) {       //checking email if it's there
    $emailErr = "Email is required.";
  } else {
    $email = test_input($_POST["email"]);     //sending email to check for spaces and etc.
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {  //checks if email is valid
      $emailErr = "Invalid email format"; 
    }
  }
  
  if (empty($_POST["message"])) {       //checking if message is written
    $messageErr = "Please write a message.";  
  } else {
    $message = test_input($_POST["message"]);    //sending to check if message has unnecessary spaces, slashes and etc
  }

  function test_input($data) {      //validating data to not have additional white spaces, html characters and slashes
    $data = trim($data);            //removing spaces before and after data
    $data = stripslashes($data);    //removing slashes
    $data = htmlspecialchars($data);      //changing html special characters
    $data = preg_replace('/\s+/', ' ',$data);       //removing unnecessary spaces between words
    return $data;               
  }


    if(isset($_POST['send'])){          //if send was pressed continue
        if($full_name && $email && $message){      //if name, email, message was got continue
          $to = "admin@kolmisoft.com"; // this is admin Email address
          $from = $_POST['email']; // this is the sender's Email address
          $subject = "Form submission";       //subject to admin email
          $subject2 = "Copy of your form submission";           //sender's subject
          $emailMessage = $full_name . " wrote the following:" . "\n\n" . $message;       //message for admin
          $emailMessage2 = "Here is a copy of your message " . $full_name . "\n\n" . $message;    //message for sender
          $headers = "From:" . $from;                 //header for admin
          $headers2 = "From:" . $to;                  //header for sender
          mail($to,$subject,$emailMessage,$headers);
          mail($from,$subject2,$emailMessage2,$headers2); // sends a copy of the message to the sender
          echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";      
              //after successful insert show alert and redirect  back to contact.php page
              $_SESSION["full_nameErr"] = '';
              $_SESSION["emailErr"] = '';
              $_SESSION["messageErr"] = '';
              $_SESSION["successful"] = 'Your message was send successfully';
            header("Location: /kolmisoft/contact.php"); 
            exit();                
        }else{ 
            if($full_nameErr && $emailErr && $messageErr){
            //if name and email and message is bad show alert and redirect back to page
              $_SESSION["full_nameErr"] = $full_nameErr;
              $_SESSION["emailErr"] = $emailErr;
              $_SESSION["messageErr"] = $messageErr;
              $_SESSION["successful"] = '';
              header("Location: /kolmisoft/contact.php");
              exit();
            }
            if($full_nameErr && $emailErr){
            //if name and email is bad show alert and redirect back to page
                $_SESSION["full_nameErr"] = $full_nameErr;
                $_SESSION["emailErr"] = $emailErr;
                $_SESSION["successful"] = '';
                header("Location: /kolmisoft/contact.php");
                exit();
              }
            if($full_nameErr && $messageErr){
            //if name and message is bad show alert and redirect back to page
                $_SESSION["full_nameErr"] = $full_nameErr;
                $_SESSION["messageErr"] = $messageErr;
                $_SESSION["successful"] = '';
                header("Location: /kolmisoft/contact.php");
                exit();
              }
            if($emailErr && $messageErr) {
            //if email and message is bad show alert and redirect back to page
                $_SESSION["emailErr"] = $emailErr;
                $_SESSION["messageErr"] = $messageErr;
                $_SESSION["successful"] = '';
                header("Location: /kolmisoft/contact.php");
                exit();
              }       
        }
    }
?>


