
//for drowdown meniu
function dropdownFunction() {
    document.getElementById("dropdown").classList.toggle("show");
  }
  
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbutton')) {
      let dropdowns = document.getElementsByClassName("meniu-dropdown");
      let i;
      for (i = 0; i < dropdowns.length; i++) {
        let openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }

//for bolding a header's "a" when you are in that page
$(function(){
  // this will get the full URL at the address bar
  let url = window.location.href; 

  // passes on every "a" tag 
  $(".navbar a").each(function() {
          // checks if its the same on the address bar
      if(url == (this.href)) { 
          $(this).closest("li").addClass("active");
      }

  });
});

function mobileMeniu() {
  let meniu = document.getElementById("meniu");
  if (meniu.style.display === "block") {
    meniu.style.display = "none";
  } else {
    meniu.style.display = "block";
  }
}


