<?php 
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kolmisoft</title>
    <link rel="shortcut icon" href="#">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    <link rel="stylesheet" type="text/css" media="(min-width: 320px) and (max-width: 519px)" href="css/mobile-style.css">
    <link rel="stylesheet" type="text/css" media="(min-width: 520px) and (max-width: 830px)" href="css/mobile-landscape-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body id="container">
    <!-- uploading a header -->
    <?php include "./header.html" ?>
    <!-- map block -->
    <div class="map-block">         
        <img class="map" src="images/map.png">
    </div>
    <!-- contact block where is information about company and high role persons -->
    <div class="contacts">
        <div class="contact-intro">
            <div id="contact">Contacts</div>
            <div><span>Home//Contacts<span></div>
        </div>
        <div class="other-info">
            <div class="company-block">
                <div class="name">Kolmisoft</div>
                <span>Kolmisoft UAB <br>
                Company code: 301534711</span> <br>
                VAT payer code: <span>LT100003884617</span> <br>
                DUNS: <span>681694042</span>
            </div>
            <div class="hours-block">
                <div class="name">Working hours</div>
                <div class="add-space">I-V: <span>9:00 - 18:00 (9am-6pm) GMT+2</span><br></div>
                Phone: <span>+37052058393</span> <br>
                Fax: <span>+37052058392</span> <br>
            </div>
            <div class="address-block">
                <div class="name">Address</div>
                <span>Kareiviu 6-405 Vilnius, 09117 <br>
                Lithuania </span>
            </div>
            <div class="support-block">
                <div class="name">Support system</div>
                <span>In case of any issues, follow this link:</span> <br>
                <a href="#" target="_blank" >support.kolmisoft.com </a> 
            </div>
            <div class="person1-block">
                <img class="photo" src="images/zmogus1.png">
                <div class="name">Mindaugas Kezys</div>
                <div class="role">Managing director</div>
                <span>E:</span> <a href="mailto:mindaugas@kolmisoft.com"  >mindaugas@kolmisoft.com </a> <br> 
                <a href="#"><img class="social-networks" src="images/linkedin.png"></a>
            </div>
            <div class="person2-block">
                <img class="photo" src="images/zmogus2.png">
                <div class="name">Vilius Stanislovaitis</div>
                <div class="role">Chief sales officer</div>
                <span>E:</span> <a href="mailto:vilius@kolmisoft.com"  >vilius@kolmisoft.com </a> <br> 
                <a href="#"><img class="social-networks" src="images/linkedin.png"></a>
                <a href="#"><img class="social-networks" src="images/skype.png"></a>
            </div>
        </div>
    </div>
    <!-- form to send a message and then "try out products" block -->
    <div id="contact-form-products">
       <div id="contact-form">
            <div id="form-name">Contact form</div>
            <div><span>Fill up the form or email us directly</span> <a href="mailto:info@kolmisoft.com">info@kolmisoft.com</a></div>
            <form id="form" method="POST" action="sendMessage.php">
                <?php if(isset($_SESSION["full_nameErr"])) { echo "<span class='error'>" . $_SESSION["full_nameErr"] . "</span>"; }?>
                <input type="text" name="full_name" placeholder="Full name *" >
                <?php if(isset($_SESSION["emailErr"])) { echo "<span class='error'>" . $_SESSION["emailErr"] . "</span>"; }?> 
                <input type="email" name="email" placeholder="Email *" >
                <?php if(isset($_SESSION["messageErr"])) { echo "<span class='error'>" . $_SESSION["messageErr"] . "</span>"; }?>
                <textarea rows="2" name="message" placeholder="Message *" ></textarea> 
                <input type="submit" name="send" value="Send message" id="send-button">
                <?php if(isset($_SESSION["successful"])) { echo "<span class='success'>" . $_SESSION["successful"] . "</span>"; }?>
            </form>
       </div>
       <div id="try-products">
            <div id="product-main">Try our products</div>
            <div id="tryout1">
                <div class="product-name">Try Mor CLASS 5</div>
                <div class="product-for">For Retail & Wholesale</div>
                <a href="#" class="request-button"> Request Demo </a>
            </div>
            <div id="tryout2">
                <div class="product-name">Try Mor CLASS 4</div>
                <div class="product-for">For Wholesale Transit</div>
                <a href="#" class="request-button"> Request Demo </a>
            </div>
       </div>
    </div>
    <!-- uploading a footer -->
    <?php include "./footer.html" ?>
</body>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script type="text/javascript" src="js/functions.js"></script>
</html>
